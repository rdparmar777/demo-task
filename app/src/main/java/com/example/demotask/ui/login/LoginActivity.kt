package com.example.demotask.ui.login

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.example.demotask.data.model.User
import com.example.demotask.databinding.ActivityLoginBinding
import com.example.demotask.ui.main.MainActivity
import com.example.demotask.utils.*
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLoginBinding
    private val loginViewModel by viewModels<LoginViewModel>()
    private lateinit var mContext: Context

    companion object {
        private const val TAG = "LoginActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initData()
    }

    private fun initData() {
        mContext = this

        binding.apply {
            fabLogin.setOnClickListener {
                if (Validator.isLoginDataValid(edtEmail, edtPassword, tnlEmail, tnlPassword)) {
                    val email = edtEmail.text.toString()
                    loginViewModel.login(User(email, edtPassword.text.toString()))
                    edtEmail.setEditableText("")
                    edtPassword.setEditableText("")
                    showToastLong("${getGreetingMessage()} ${email.split("@")[0]}")
                    Handler(Looper.getMainLooper()).postDelayed({
                        startNewActivityAndFinish(mContext, MainActivity::class.java)
                    }, 1500)
                }
            }
        }
    }
}