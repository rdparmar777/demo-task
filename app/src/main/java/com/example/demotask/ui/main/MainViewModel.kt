package com.example.demotask.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.example.demotask.data.model.User
import com.example.demotask.data.repository.UserRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val userRepository: UserRepository) : ViewModel() {
    val getLoggedInUsers: LiveData<List<User>>
        get() = userRepository.getUser.flowOn(Dispatchers.Main).asLiveData(context = viewModelScope.coroutineContext)
}