package com.example.demotask.ui.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.demotask.data.model.User
import com.example.demotask.data.repository.UserRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class LoginViewModel @Inject constructor(private val userRepository: UserRepository) : ViewModel() {
    fun login(user: User) {
        viewModelScope.launch {
            userRepository.insert(user)
        }
    }
}