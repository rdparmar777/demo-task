package com.example.demotask.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.demotask.R
import com.example.demotask.data.model.Drug
import com.example.demotask.databinding.ItemDrugBinding

class JSONAdapter(private val context: Context, private val drugList: ArrayList<Drug>) : RecyclerView.Adapter<JSONAdapter.JSONViewHolder>() {

    class JSONViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = ItemDrugBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JSONViewHolder {
        return JSONViewHolder(LayoutInflater.from(context).inflate(R.layout.item_drug, parent, false))
    }

    override fun onBindViewHolder(holder: JSONViewHolder, position: Int) {
        holder.binding.apply {
            val drug = drugList[position]
            drugData = Drug(
                context.getString(R.string.drug_dose, drug.dose),
                context.getString(R.string.drug_name, drug.name),
                context.getString(R.string.drug_strength, drug.strength)
            )
        }
    }

    override fun getItemCount(): Int {
        return drugList.size
    }
}
