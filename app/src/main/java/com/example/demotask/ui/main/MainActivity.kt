package com.example.demotask.ui.main

import android.content.Context
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.demotask.R
import com.example.demotask.data.model.User
import com.example.demotask.databinding.ActivityMainBinding
import com.example.demotask.ui.adapter.UserAdapter
import com.example.demotask.ui.json.JSONViewActivity
import com.example.demotask.utils.startNewActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private var userAdapter: UserAdapter? = null
    private val mainViewModel by viewModels<MainViewModel>()
    private lateinit var mContext: Context
    private val userList = ArrayList<User>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initData()
    }

    private fun initData() {
        mContext = this
        userAdapter = UserAdapter(mContext, userList)
        binding.headerLayout.tvTitle.text = getString(R.string.users)

        binding.apply {
            fabJSON.setOnClickListener { startNewActivity(mContext, JSONViewActivity::class.java) }

            recyclerView.apply {
                layoutManager = LinearLayoutManager(mContext)
                adapter = userAdapter
            }
        }

        mainViewModel.getLoggedInUsers.observe(this, Observer {
            userList.clear()
            userList.addAll(it)
            userAdapter?.notifyItemInserted(it.size - 1)
        })
    }
}