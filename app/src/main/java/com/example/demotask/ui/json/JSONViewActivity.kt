package com.example.demotask.ui.json

import android.content.Context
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.demotask.R
import com.example.demotask.data.model.Drug
import com.example.demotask.databinding.ActivityMainBinding
import com.example.demotask.ui.adapter.JSONAdapter
import com.example.demotask.utils.getJsonDataFromAsset


class JSONViewActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var mContext: Context
    private val drugList = ArrayList<Drug>()
    private val jsonViewModel by viewModels<JSONViewModel>()
    var jsonAdapter: JSONAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initData()
    }

    private fun initData() {
        mContext = this
        jsonAdapter = JSONAdapter(mContext, drugList)

        binding.apply {
            headerLayout.apply {
                imgBack.setOnClickListener { onBackPressed() }
                tvTitle.text = getString(R.string.json_data)
            }

            recyclerView.apply {
                layoutManager = LinearLayoutManager(mContext)
                adapter = jsonAdapter
            }

            fabJSON.hide()
        }

        jsonViewModel.loadJSONList(getJsonDataFromAsset("JsonData.json"))

        jsonViewModel.drugListMutableLiveData.observe(this) {
            drugList.clear()
            drugList.addAll(it)
            jsonAdapter?.notifyItemInserted(drugList.size - 1)
        }
    }
}