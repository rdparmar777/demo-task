package com.example.demotask.ui.json

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.demotask.data.model.Drug
import com.google.gson.Gson
import dagger.hilt.android.lifecycle.HiltViewModel
import org.json.JSONArray
import org.json.JSONObject
import javax.inject.Inject

@HiltViewModel
class JSONViewModel @Inject constructor() : ViewModel() {
    val drugListMutableLiveData = MutableLiveData<ArrayList<Drug>>()

    fun loadJSONList(jsonDataFromAsset: String?) {
        val drugList = ArrayList<Drug>()
        try {
            var json = JSONObject(jsonDataFromAsset)
            var iterator = json.keys()

            if (iterator != null) {
                while (iterator.hasNext()) {
                    val key = iterator.next()
                    val value = json[key]
                    val dataType = value.javaClass.simpleName
                    when {
                        dataType.equals("JSONArray", true) -> {
                            val array = json.getJSONArray(key)
                            json = array[0] as JSONObject
                            var iterator1 = json.keys()
                            while (iterator1.hasNext()) {
                                val key1 = iterator1.next()
                                val value1 = json[key1] as JSONArray
                                val jsonValue = value1[0] as JSONObject

                                if (jsonValue.has("medications")) {
                                    val medicationObject = jsonValue.getJSONArray("medications")[0] as JSONObject
                                    val medicationClassesObject = medicationObject.getJSONArray("medicationsClasses")[0] as JSONObject
                                    for (key in medicationClassesObject.keys()) {
                                        val classObject = (medicationClassesObject[key] as JSONArray)[0] as JSONObject
                                        for (drugKeys in classObject.keys()) {
                                            val data = (classObject[drugKeys] as JSONArray)[0] as JSONObject
                                            val jsonData = Gson().fromJson(data.toString(), Drug::class.java)
                                            drugList.add(jsonData)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        drugListMutableLiveData.value = drugList
    }
}