package com.codingwithjks.roomdatabasewithflow.di

import android.content.Context
import androidx.room.Room
import com.example.demotask.data.database.UserDatabase
import com.example.demotask.data.local.UserDao
import com.example.demotask.data.repository.UserRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    @Provides
    fun providesUserDao(userDatabase: UserDatabase): UserDao = userDatabase.userDao()

    @Provides
    @Singleton
    fun providesUserDatabase(@ApplicationContext context: Context): UserDatabase = Room.databaseBuilder(context, UserDatabase::class.java, "UserDatabase").build()

    @Provides
    fun providesUserRepository(userDao: UserDao): UserRepository = UserRepository(userDao)
}