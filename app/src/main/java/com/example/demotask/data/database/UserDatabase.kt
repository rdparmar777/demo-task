package com.example.demotask.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.demotask.data.local.UserDao
import com.example.demotask.data.model.User

@Database(entities = [User::class], version = 1, exportSchema = false)
abstract class UserDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
}
