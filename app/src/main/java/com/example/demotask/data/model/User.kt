package com.example.demotask.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "UserTable")
class User(
    @ColumnInfo(name = "user_email")
    val userEmail: String,

    @ColumnInfo(name = "user_password")
    val userPassword: String
) {
    @PrimaryKey(autoGenerate = true)
    var userID: Int = 0
}