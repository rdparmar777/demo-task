package com.example.demotask.data.model

data class Drug(
    val dose: String = "",
    val name: String = "",
    val strength: String = ""
)