package com.example.demotask.utils

import android.util.Log
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import java.util.regex.Matcher
import java.util.regex.Pattern


class Validator {
    companion object {
        private val TAG = "Validator"

        fun isLoginDataValid(editEmail: TextInputEditText, editPassword: TextInputEditText, inputEmail: TextInputLayout, inputPassword: TextInputLayout): Boolean {
            return isEmailValid(editEmail, inputEmail) && isPasswordValid(editPassword, inputPassword)
        }

        private fun isPasswordValid(editPassword: TextInputEditText, inputPassword: TextInputLayout): Boolean {
            when {
                editPassword.text == null -> {
                    inputPassword.error = "Please enter password"
                }
                editPassword.text.toString() == "" -> {
                    inputPassword.error = "Please enter password"
                }
                editPassword.text.toString().length < 6 -> {
                    inputPassword.error = "Password must contain 6 characters"
                }
                else -> {
                    inputPassword.error = null
                    Log.d(TAG, "Password is valid")
                    return true
                }
            }
            return false
        }

        private fun isEmailValid(editEmail: TextInputEditText, inputEmailAddress: TextInputLayout): Boolean {
            var isValid = false
            when {
                editEmail.text == null -> {
                    inputEmailAddress.error = "Please enter email"
                }
                editEmail.text.toString() == "" -> {
                    inputEmailAddress.error = "Please enter email"
                }
                !isEmailValids(editEmail.text.toString()) -> {
                    inputEmailAddress.error = "Email is not valid"
                }
                else -> {
                    inputEmailAddress.error = null
                    Log.d(TAG, " Email is valid")
                    isValid = true
                }
            }
            return isValid
        }

        private fun isEmailValids(email: String): Boolean {
            val expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
            val pattern: Pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
            val matcher: Matcher = pattern.matcher(email)
            return matcher.matches()
        }

    }
}