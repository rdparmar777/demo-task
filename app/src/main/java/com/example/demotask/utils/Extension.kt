package com.example.demotask.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.text.Editable
import android.widget.Toast
import com.google.android.material.textfield.TextInputEditText
import java.io.IOException
import java.util.*

private const val TAG = "Extension"

fun Activity.startNewActivity(context: Context, java: Class<*>) {
    startActivity(Intent(context, java))
}

fun Activity.startNewActivityAndFinish(context: Context, java: Class<*>) {
    startActivity(Intent(context, java))
    finish()
}

fun Context.showToastShort(msg: String) {
    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
}

fun Context.showToastLong(msg: String) {
    Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
}

fun TextInputEditText.setEditableText(string: String) {
    this.text = Editable.Factory.getInstance().newEditable(string)
}

fun getGreetingMessage(): String {
    val c = Calendar.getInstance()
    return when (c.get(Calendar.HOUR_OF_DAY)) {
        in 0..11 -> "Good Morning"
        in 12..15 -> "Good Afternoon"
        in 16..20 -> "Good Evening"
        in 21..23 -> "Good Night"
        else -> "Hello"
    }
}

fun Context.getJsonDataFromAsset(fileName: String): String? {
    val jsonString: String
    try {
        jsonString = this.assets.open(fileName).bufferedReader().use { it.readText() }
    } catch (ioException: IOException) {
        ioException.printStackTrace()
        return null
    }
    return jsonString
}
